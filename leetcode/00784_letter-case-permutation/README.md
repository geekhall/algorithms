# 00784. Letter Case Permutation

DescriptionGiven a string s, we can transform every letter individually to be lowercase or uppercase to create another string.

Return a list of all possible strings we could create. You can return the output in any order.

## Example 1

```txt
Input: s = "a1b2"
Output: ["a1b2","a1B2","A1b2","A1B2"]
```

## Example 2

```txt
Input: s = "3z4"
Output: ["3z4","3Z4"]
```

## Example 3

```txt
Input: s = "12345"
Output: ["12345"]
```

## Example 4

```txt
Input: s = "0"
Output: ["0"]
```

## Constraints

```txt
s will be a string with length between 1 and 12.
s will consist only of letters or digits.
```

## Solution 1

### Go

```go
func letterCasePermutation(s string) []string {
    
}
```
