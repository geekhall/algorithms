# 00007. Reverse Integer

Given a signed 32-bit integer x, return x with its digits reversed. If reversing x causes the value to go outside the signed 32-bit integer range [-231, 231 - 1], then return 0.

Assume the environment does not allow you to store 64-bit integers (signed or unsigned).

## 题目大意

## Example 1

```txt
Input: x = 123
Output: 321
```

## Example 2

```txt
Input: x = -123
Output: -321
```

## Example 3

```txt
Input: x = 120
Output: 21
```

## Example 4

```txt
Input: x = 0
Output: 0
```

## Constraints

$-2^{31} <= x <= 2^{31} - 1$

## Solution 1

### Go

```go

```
