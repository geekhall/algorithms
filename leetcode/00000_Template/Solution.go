package main

import "fmt"

// Solution 1
func solution1(numbers []int, target int) []int {

	return []int{}
}

// Solution 2
func solution2(numbers []int, target int) []int {

	return []int{}
}

func main() {
	test_data := []int{2, 3, 7, 9, 11}
	res := solution1(test_data, 9)
	fmt.Println(res)
}
