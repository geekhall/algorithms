# 00077. Combinations

Given two integers n and k, return all possible combinations of k numbers out of the range [1, n].

You may return the answer in any order.

## 题目大意

给定两个整数 n 和 k，返回范围 [1, n] 中所有可能的 k 个数的组合。

你可以按 任何顺序 返回答案。

## Example 1

```txt
Input: n = 4, k = 2
Output:
[
  [2,4],
  [3,4],
  [2,3],
  [1,2],
  [1,3],
  [1,4],
]
```

## Example 2

```txt
Input: n = 1, k = 1
Output: [[1]]
```

## Constraints

```txt
1 <= n <= 20
1 <= k <= n
```

## Solution 1

### Go

```go

```
