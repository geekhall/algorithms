# 00050. Pow

Implement pow(x, n), which calculates x raised to the power n (i.e., xn).

## 题目大意

实现 pow(x, n) ，即计算 x 的 n 次幂函数（即，$x^n$）。

## Example 1

```txt
Input: x = 2.00000, n = 10
Output: 1024.00000
```

## Example 2

```txt
Input: x = 2.10000, n = 3
Output: 9.26100
```

## Example 3

```txt
Input: x = 2.00000, n = -2
Output: 0.25000
Explanation: 2-2 = 1/22 = 1/4 = 0.25
```

## Constraints

```txt
-100.0 < x < 100.0
-231 <= n <= 231-1
-104 <= xn <= 104
```

## Solution 1

### Go

```go
func myPow(x float64, n int) float64 {

}
```
