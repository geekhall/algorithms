# 00046. Permutations

Given an array nums of distinct integers, return all the possible permutations. You can return the answer in any order.

## 题目大意

给定一个不含重复数字的数组 `nums` ，返回其 `所有可能的全排列` 。你可以 `按任意顺序` 返回答案。

## Example 1

```txt
Input: nums = [1,2,3]
Output: [[1,2,3],[1,3,2],[2,1,3],[2,3,1],[3,1,2],[3,2,1]]
```

## Example 2

```txt
Input: nums = [0,1]
Output: [[0,1],[1,0]]
```

## Example 3

```txt
Input: nums = [1]
Output: [[1]]
```

## Constraints

```txt
1 <= nums.length <= 6
-10 <= nums[i] <= 10
All the integers of nums are unique.
```

## Solution 1

### Go

```go

```
